# Modify accordingly
# Define <PATH> according to your system
file_confirmed = '<PATH>/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv'
file_deaths = '<PATH>/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Deaths.csv'
file_recovered = '<PATH>/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Recovered.csv'

repo_path = '<REPO_PATH>'
fig_path = '<FIGURES_PATH>'

import csv
import pandas as pd
"""
from git import Repo
# https://github.com/CSSEGISandData/COVID-19
repo = Repo('<REPO_PATH>')
o = repo.remotes.origin
o.pull()
"""

POPULATIONS = {'China' : 1408526449,
               'Switzerland' : 8654622,
               'United Kingdom' : 67886011,
               'US' : 331002651,
               'Austria' : 8858775,
               'Belgium' : 11467923,
               'Bulgaria' : 7000039,
               'Croatia' : 4076246,
               'Cyprus' : 875898,
               'Czechia' : 10649800,
               'Denmark' : 5806081,
               'Estonia' : 1324820,
               'Finland' : 5517919,
               'France' : 67028048,
               'Germany' : 83019214,
               'Greece' : 10722287,
               'Hungary' : 9772756,
               'Ireland' : 4904226,
               'Italy' : 60359546,
               'Latvia' : 1919968,
               'Lithuania' : 2794184,
               'Luxembourg' : 613894,
               'Malta' : 493559,
               'Netherlands' : 17282163,
               'Poland' : 37972812,
               'Portugal' : 10276617,
               'Romania' : 19401658,
               'Slovakia' : 5450421,
               'Slovenia' : 2080908,
               'Spain' : 46934632,
               'Sweden' : 10230185}

def read_data(file_confirmed, file_deaths, file_recovered):
    data = {}
    countries = set()

    with open(file_confirmed, newline='') as csv_confirmed, open(file_deaths, newline='') as csv_deaths, open(file_recovered, newline='') as csv_recovered:

        reader_confirmed = csv.reader(csv_confirmed, delimiter=',')#, quoting=csv.QUOTE_ALL, skipinitialspace=True)
        reader_deaths = csv.reader(csv_deaths, delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True)
        reader_recovered = csv.reader(csv_recovered, delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True)

        for row, (confirmed, deaths, recovered) in enumerate(zip(reader_confirmed, reader_deaths, reader_recovered)):
            if row == 0:
                if (confirmed == deaths and confirmed == recovered):
                    print("Headers match!")
                else:
                    print("Warning: headers do not match!")
            else:
                countries.add(confirmed[1])
                if confirmed[1] + ' Confirmed' in data.keys():
                    data.update({confirmed[1] + ' Confirmed' : [sum(x) for x in zip(data[confirmed[1] + ' Confirmed'], [int(c) for c in confirmed[4:]])]})
                    data.update({confirmed[1] + ' Deaths' : [sum(x) for x in zip(data[confirmed[1] + ' Deaths'], [int(c) for c in deaths[4:]])]})
                    data.update({confirmed[1] + ' Recovered' : [sum(x) for x in zip(data[confirmed[1] + ' Recovered'], [int(c) for c in recovered[4:]])]})
                else:
                    data[confirmed[1] + ' Confirmed'] = [int(c) for c in confirmed[4:]]
                    data[confirmed[1] + ' Deaths'] = [int(d) for d in deaths[4:]]
                    data[confirmed[1] + ' Recovered'] = [int(r) for r in recovered[4:]]

    return data, countries

def truncate_leading_zeroes(data, countries):
    data_truncated = {}
    max_length = 0

    for country in countries:
        for i, item in enumerate(data[country + ' Confirmed']):
            # Get the day 1 index and truncate data accordingly
            if item != 0:
                data_truncated[country + ' Confirmed'] = data[country + ' Confirmed'][i:]
                if country + ' Deaths' in data.keys():
                    data_truncated[country + ' Deaths'] = data[country + ' Deaths'][i:]
                if country + ' Recovered' in data.keys():
                    data_truncated[country + ' Recovered'] = data[country + ' Recovered'][i:]

                # Get max list length for future padding
                if len(data[country + ' Confirmed']) > max_length:
                    max_length = len(data[country + ' Confirmed'])
                break

    return max_length, data_truncated

def add_cases_per_capita(data_truncated, POPULATIONS):
    for country in POPULATIONS.keys():
        if country + ' Confirmed' in data_truncated.keys():
            data_truncated[country + ' CpC'] = [(100*x)/POPULATIONS[country] for x in data_truncated[country + ' Confirmed']]
    return data_truncated

def add_deaths_per_capita(data_truncated, POPULATIONS):
    for country in POPULATIONS.keys():
        if country + ' Deaths' in data_truncated.keys():
            data_truncated[country + ' DpC'] = [(100*x)/POPULATIONS[country] for x in data_truncated[country + ' Deaths']]
    return data_truncated

def add_death_confirmed_ratio(data_truncated, POPULATIONS):
    for country in POPULATIONS.keys():
        if country + ' Confirmed' in data_truncated.keys() and country + ' Deaths' in data_truncated.keys():
            data_truncated[country + ' Mortality Rate'] = [(100*d)/c for d,c in zip(data_truncated[country + ' Deaths'],data_truncated[country + ' Confirmed'])]
    return data_truncated

def add_active_cases_per_capita(data_truncated, POPULATIONS):
    for country in POPULATIONS.keys():
        if country + ' Deaths' in data_truncated.keys():
            deaths = data_truncated[country + ' Deaths']
        else:
            deaths = [0]*len(data_truncated[country + ' Confirmed'])

        if country + ' Recovered' in data_truncated.keys():
            recovered = data_truncated[country + ' Recovered']
        else:
            recovered = [0]*len(data_truncated[country + ' Confirmed'])

        data_truncated[country + ' Active CpC'] = [100*(c-d-r)/POPULATIONS[country] for c,d,r in zip(data_truncated[country + ' Confirmed'], deaths, recovered)]
    return data_truncated

def add_resolved_cases(data_truncated, countries):
    for key in data_truncated.keys():
        if "Guam" in key or "Guernsey" in key:
            print(key)

    for country in countries:
        if country + ' Deaths' in data_truncated.keys():
            deaths_avg = data_truncated[country + ' Deaths']
            deaths = [d-data_truncated[country + ' Deaths'][i-1] if i > 1 else d for i, d in enumerate(data_truncated[country + ' Deaths'])]
        else:
            deaths_avg = [0]*len(data_truncated[country + ' Confirmed'])
            deaths = [0]*len(data_truncated[country + ' Confirmed'])

        if country + ' Recovered' in data_truncated.keys():
            recovered_avg = data_truncated[country + ' Recovered']
            recovered = [r-data_truncated[country + ' Recovered'][i-1] if i > 1 else r for i, r in enumerate(data_truncated[country + ' Recovered'])]
        else:
            recovered_avg = [0]*len(data_truncated[country + ' Confirmed'])
            recovered = [0]*len(data_truncated[country + ' Confirmed'])

        total = [d+r for d,r in zip(deaths,recovered)]
        total_avg = [d+r for d,r in zip(deaths_avg,recovered_avg)]

        data_truncated[country + ' RR'] = [100*(r/t) if t > 0 else None for t,r in zip(total,recovered)]
        data_truncated[country + ' RD'] = [100*(d/t) if t > 0 else None for t,d in zip(total,deaths)]

        data_truncated[country + ' RR Avg.'] = [100*(r/t) if t > 0 else None for t,r in zip(total_avg,recovered_avg)]
        data_truncated[country + ' RD Avg.'] = [100*(d/t) if t > 0 else None for t,d in zip(total_avg,deaths_avg)]

    return data_truncated

def pad_or_truncate(data, max_length):
    data_padded = {}
    for entity in data.keys():
        data_padded[entity] = data[entity][:max_length] + [None]*(max_length - len(data[entity]))
    return data_padded

# Read data
data, countries = read_data(file_confirmed, file_deaths, file_recovered)
# Truncate
max_length, data_truncated = truncate_leading_zeroes(data, countries)
# Add confirmed cases per capita
data_truncated = add_cases_per_capita(data_truncated, POPULATIONS)
# Add deaths per capita
data_truncated = add_deaths_per_capita(data_truncated, POPULATIONS)
# Add mortality rate
data_truncated = add_death_confirmed_ratio(data_truncated, POPULATIONS)
# Add active cases per capita
data_truncated = add_active_cases_per_capita(data_truncated, POPULATIONS)
# Add resolved cases
data_truncated = add_resolved_cases(data_truncated, countries)
# Add padding
data_padded = pad_or_truncate(data_truncated, max_length)

"""
df = pd.DataFrame(data_padded)
writer = pd.ExcelWriter('output.xlsx'), engine='xlsxwriter')
df.to_excel(writer, sheet_name='Overview', index_label = 'Day')
writer.save()
"""