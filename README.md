# COVID-19 Day Zero Analysis

COVID-19 Day Zero Analysis based on the data from https://github.com/CSSEGISandData/COVID-19

Quick look at the data from a different point of view. Few bold assumptions were made in the code, for example that the data in all three files has exactly the same structure. Always check your data, adapt accordingly.